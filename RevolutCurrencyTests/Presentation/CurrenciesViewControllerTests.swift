//
//  CurrenciesViewControllerTests.swift
//  RevolutCurrencyTests
//
//  Created by Michael Nikolaev on 25/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import XCTest
@testable import RevolutCurrency

final class CurrenciesViewControllerTests: XCTestCase {
    
    // MARK: - Property
    
    var viewController: CurrenciesViewController!
    
    // MARK: - XCTestCase
    
    override func setUp() {
        super.setUp()
        let dataSource = CurrenciesDataSource()
        dataSource.currencies = [
            ("EUR", 1.55),
            ("AUD", 99.1),
            ("THB", 12.2)
        ]
        viewController = CurrenciesViewController(dataSource: dataSource)
    }
    
    // MARK: - Public methods
    
    func testRowsToReload() {
        XCTAssertEqual(viewController.rowsToReload.count, 2)
        XCTAssertEqual(viewController.rowsToReload[0],
                       IndexPath(row: 1, section: 0))
        XCTAssertEqual(viewController.rowsToReload[1],
                       IndexPath(row: 2, section: 0))
    }
}
