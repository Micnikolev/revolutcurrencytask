//
//  CurrenciesTableViewCellTests.swift
//  RevolutCurrencyTests
//
//  Created by Michael Nikolaev on 25/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import XCTest
@testable import RevolutCurrency

final class CurrenciesTableViewCellTests: XCTestCase {
    
    // MARK: - Property
    
    var viewController: CurrenciesViewController!
    var window: UIWindow!
    
    // MARK: - XCTestCase
    
    override func setUp() {
        super.setUp()
        window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyboard.instantiateViewController(withIdentifier: "CurrenciesViewController") as? CurrenciesViewController
        window.rootViewController = viewController
    }
    
    // MARK: - Public methods
    
    func testFillCell() {
        viewController.reload(currencies: CurrenciesDto(base: "EUR",
                                                        date: "2.01.2012",
                                                        rates: ["AUD": 2.4]))
        let cell = viewController.tableView.dequeueReusableCell(
            withIdentifier: TableViewConstants.currencyTableViewCellIdentifier,
            for: IndexPath(row: 0, section: 0)) as! CurrencyTableViewCell
        
        cell.fill(title: "EUR", currency: 123.2, multiplier: 2, editingEnabled: false)
        XCTAssertEqual(cell.titleLabel.text, "EUR")
        XCTAssertEqual(cell.currencyTextField.text, "246.4")
    }
}
