//
//  ServiceTests.swift
//  RevolutCurrencyTests
//
//  Created by Michael Nikolaev on 25/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import XCTest
@testable import RevolutCurrency

final class ServiceTests: XCTestCase {
    
    // MARK: - Property
    
    let service = Service()
    let currencyService = ServiceLayer.instance.currenciesService
    
    // MARK: - Public methods
    
    func testMakeURL() {
        let desiredUrl = URL(string: "https://revolut.duckdns.org/latest?base=EUR")
        let query = URLQueryItem(name: currencyService.queryName, value: "EUR")
        let serviceUrl = service.makeURL(from: currencyService.currenciesPath, queryItems: [query])
        XCTAssertEqual(serviceUrl, desiredUrl)
    }
}
