//
//  CurrenciesTests.swift
//  RevolutCurrencyTests
//
//  Created by Michael Nikolaev on 25/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import XCTest
@testable import RevolutCurrency

final class CurrenciesTests: XCTestCase {
    
    // MARK: - Property
    
    var bundle: Bundle { return Bundle(for: CurrenciesTests.self) }
    var currencies: CurrenciesDto!
    
    // MARK: - Public methods
    
    func testDecode() {
        do {
            let result = try parseResult()
            
            XCTAssertEqual(result.base, "EUR")
            XCTAssertEqual(result.date, "2018-09-06")
            XCTAssertEqual(result.rates["AUD"], 1.6159)
            XCTAssertEqual(result.rates.count, 32)
        } catch {
            XCTFail("\(error)")
        }
    }
    
    // MARK: - Helper
    
    private func parseResult() throws -> CurrenciesDto {
        let url = bundle.url(forResource: "currencies", withExtension: "json")!
        let data = try Data(contentsOf: url)
        return try JSONDecoder().decode(CurrenciesDto.self, from: data)
    }
}
