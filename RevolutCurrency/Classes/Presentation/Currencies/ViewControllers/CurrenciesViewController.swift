//
//  CurrenciesViewController.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

/// Controller with the list of currencies
final class CurrenciesViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Public properties
    
    /// Reloading all rows in tableView besides the first
    lazy var rowsToReload: [IndexPath] = {
        return (1..<self.dataSource.currencies.count).map { IndexPath(row: $0, section: 0) }
    }()
    
    // MARK: - Private properties
    
    private var baseCurrency = "EUR"
    private var timer: Timer?
    private var dataSource = CurrenciesDataSource()
    private let currencyFetchingTimeInterval = TimeInterval(1)
    private let currenciesService = ServiceLayer.instance.currenciesService
    private var isKeyboardActive = false
    
    // MARK: - Initialization
    
    convenience init(dataSource: CurrenciesDataSource) {
        self.init(nibName: nil, bundle: nil)
        self.dataSource = dataSource
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currencyNib = UINib(
            nibName: TableViewConstants.currencyTableViewCellIdentifier,
            bundle: nil)
        tableView.register(currencyNib,
                           forCellReuseIdentifier: TableViewConstants.currencyTableViewCellIdentifier)
        tableView.keyboardDismissMode = .onDrag
        tableView.dataSource = dataSource
        tableView.delegate = self
        dataSource.multiplierChanged = {
            self.reloadCurrenciesTableView()
        }
        obtainCurrencies()
        initializeTimerForCurrencies()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        invalidateCurrenciesTimer()
    }
    
    // MARK: - Public methods
    
    func reload(currencies: CurrenciesDto) {
        /// Merging base currency with fetched currencies
        let baseCurrency = [self.baseCurrency: dataSource.currencyMultiplier]
        dataSource.currencies = Array(baseCurrency) + Array(currencies.rates)
        reloadCurrenciesTableView()
    }
    
    // MARK: - Private methods
    
    private func reloadCurrenciesTableView() {
        let numberOfRows = tableView.numberOfRows(inSection: 0)
        
        if numberOfRows != 0 {
            tableView.beginUpdates()
            tableView.reloadRows(at: rowsToReload, with: .none)
            tableView.endUpdates()
        } else {
            tableView.reloadData()
        }
    }
    
    @objc private func obtainCurrencies() {
        currenciesService.obtainCurrencies(
                baseCurrency: baseCurrency,
                completion: { [weak self] currencies in
                    self?.reload(currencies: currencies)
                },
                failure: { error in
                    guard let error = error else { return }
                    print(error)
                }
        )
    }

    private func initializeTimerForCurrencies() {
        timer = Timer.scheduledTimer(
            timeInterval: currencyFetchingTimeInterval,
            target: self,
            selector: #selector(self.obtainCurrencies),
            userInfo: nil,
            repeats: true)
    }
    
    private func invalidateCurrenciesTimer() {
        timer?.invalidate()
        timer = nil
    }
}

// MARK: - UITableViewDelegate

extension CurrenciesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard dataSource.currencies.count > indexPath.row else { return }
        
        /// Calculate new base currency and multiplier
        let currency = Array(dataSource.currencies)[indexPath.row]
        baseCurrency = currency.key
        dataSource.currencyMultiplier = currency.value
        
        /// Disabling of textfield of the old base cell
        let baseCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CurrencyTableViewCell
        baseCell?.editingEnabled = false
        
        /// New first cell will move and will be enabled for input
        let selectedCell = tableView.cellForRow(at: indexPath) as? CurrencyTableViewCell
        selectedCell?.editingEnabled = true
        tableView.beginUpdates()
        tableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: 0))
        tableView.endUpdates()
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0),
                              at: .top,
                              animated: true)
        
    }
}
