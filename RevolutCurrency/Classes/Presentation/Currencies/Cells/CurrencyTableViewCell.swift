//
//  CurrencyTableViewCell.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

protocol CurrencyTableViewCellDelegate: class {
    func currencyTableViewCell(_ cell: CurrencyTableViewCell,
                               multiplierChanged multiplier: Double)
}

final class CurrencyTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var currencyTextField: UITextField!
    @IBOutlet weak var underlineView: UIView!
    
    // MARK: - Property
    
    weak var delegate: CurrencyTableViewCellDelegate?
    var editingEnabled = false {
        didSet {
            currencyTextField.isEnabled = editingEnabled
            underlineView.backgroundColor = editingEnabled ? UIColor.CurrencyCell.currencyTextFieldHighlighted :
                UIColor.CurrencyCell.currencyTextFieldNormal
            if editingEnabled { showKeyboard() }
        }
    }
    
    // MARK: - UITableViewCell
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        titleLabel.textColor = UIColor.CurrencyCell.currencyTitle
        currencyTextField.textColor = UIColor.CurrencyCell.currentCurrency
        currencyTextField.keyboardType = .decimalPad
        
        currencyTextField.delegate = self
    }
    
    // MARK: - Public Methods
    
    func fill(title: String,
              currency: Double,
              multiplier: Double,
              editingEnabled: Bool) {
        let currency = Double(floor(100*currency * multiplier)/100)
        self.editingEnabled = editingEnabled
        titleLabel.text = title
        currencyTextField.text = String(currency)
        underlineView.backgroundColor = UIColor.CurrencyCell.currencyTextFieldNormal
    }
    
    func showKeyboard() {
        currencyTextField.becomeFirstResponder()
    }
    
    // MARK: - Actions
    
    @IBAction func currencyTextFieldChanged(_ sender: Any) {
        guard let text = currencyTextField.text,
            let multiplier = Double(text) else { return }
        
        delegate?.currencyTableViewCell(self, multiplierChanged: multiplier)
    }
    
    @IBAction func currencyTextFieldBeganEditing(_ sender: Any) {
        underlineView.backgroundColor = UIColor.CurrencyCell.currencyTextFieldHighlighted
    }
}

// MARK: - UITextFieldDelegate

extension CurrencyTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        underlineView.backgroundColor = UIColor.CurrencyCell.currencyTextFieldNormal
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard
            let oldText = textField.text,
            let r = Range(range, in: oldText)
            else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.index(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
    }
}
