//
//  CurrenciesDataSource.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

final class CurrenciesDataSource: NSObject {

    // MARK: - Public properties
    
    var currencies = [(key: String, value: Double)]()
    var multiplierChanged: (() -> Void)?
    
    // MARK: - Private properties
    
    var currencyMultiplier = 1.0
}

// MARK: - UITableViewDataSource

extension CurrenciesDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: TableViewConstants.currencyTableViewCellIdentifier) as? CurrencyTableViewCell,
            Array(currencies).count > indexPath.row
            else { return UITableViewCell() }
        cell.delegate = self
        cell.fill(title: currencies[indexPath.row].key,
                  currency: Array(currencies)[indexPath.row].value,
                  multiplier: currencyMultiplier,
                  editingEnabled: indexPath.row == 0)
        return cell
    }
}

// MARK: - CurrencyTableViewCellDelegate

extension CurrenciesDataSource: CurrencyTableViewCellDelegate {
    func currencyTableViewCell(_ cell: CurrencyTableViewCell,
                               multiplierChanged multiplier: Double) {
        currencyMultiplier = multiplier
        multiplierChanged?()
    }
}
