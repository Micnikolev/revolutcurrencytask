//
//  TableViewConstants.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

final class TableViewConstants {
    static let currencyTableViewCellIdentifier = "CurrencyTableViewCell"
}
