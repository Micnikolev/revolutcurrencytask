//
//  UIColor+Style.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
    enum CurrencyCell {
        static var currencyTitle: UIColor {
            return UIColor(red: 87/255, green: 87/255, blue: 87/255, alpha: 1.0)
        }
        
        static var currentCurrency: UIColor {
            return UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1.0)
        }
        
        static var currencyTextFieldNormal: UIColor {
            return UIColor(red: 223/255, green: 223/255, blue: 223/255, alpha: 1.0)
        }
        
        static var currencyTextFieldHighlighted: UIColor {
            return UIColor(red: 84/255, green: 129/255, blue: 187/255, alpha: 1.0)
        }
    }
}
