//
//  Currencies.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

typealias Currencies = [String: Double]

struct CurrenciesDto: Decodable {
    /// Base currency
    let base: String
    
    // Date of request
    let date: String
    
    // Fetched currencies
    let rates: Currencies
}
