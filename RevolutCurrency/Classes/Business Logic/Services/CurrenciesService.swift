//
//  CurrenciesService.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

final class CurrenciesService: Service {
    
    // MARK: - Private properties
    
    let currenciesPath = "/latest"
    let queryName = "base"
    
    // MARK: - Public methods
    
    func obtainCurrencies(baseCurrency: String,
                          completion: @escaping (CurrenciesDto) -> (),
                          failure: @escaping failureBlock) {
        let query = URLQueryItem(name: queryName, value: baseCurrency)
        
        self.sendRequest(path: currenciesPath,
                         queryItems: [query],
                         completion: completion,
                         failure: failure)
    }
}
