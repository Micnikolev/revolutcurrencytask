//
//  Service.swift
//  RevolutCurrency
//
//  Created by Michael Nikolaev on 24/11/2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

typealias failureBlock = (Error?) -> ()

class Service {
    
    // MARK: - Private methods
    
    private let baseURL = "revolut.duckdns.org"

    // MARK: - Public methods
    
    func sendRequest<Result: Decodable>(path: String,
                                        queryItems: [URLQueryItem],
                                        completion: @escaping (Result) -> (),
                                        failure: @escaping failureBlock) {
        guard let url = makeURL(from: path, queryItems: queryItems) else {
            failure(nil)
            return
        }
        
        let sessionRequest = URLRequest(url: url)
        
        let decoder = JSONDecoder()
        let loadSession = URLSession.shared.dataTask(with: sessionRequest) { data, _, error in
            guard let data = data else {
                failure(nil)
                return
            }
            
            /// Decoding
            do {
                let result = try decoder.decode(Result.self, from: data)
                DispatchQueue.main.async {
                    completion(result)
                }
            } catch {
                failure(error)
            }
        }
        loadSession.resume()
    }
    
    func makeURL(from path: String,
                         queryItems: [URLQueryItem]) -> URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = baseURL
        components.path = path
        components.queryItems = queryItems
        
        return components.url
    }
}
